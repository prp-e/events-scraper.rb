require 'pry'
require 'nokogiri'
require 'open-uri'

def scraper(year, month, day=nil )

    output = [] 

    doc = Nokogiri::HTML(open("https://www.time.ir/fa/event/list/0/#{year}/#{month}/#{day}"))
    events_holder = doc.css("ul.list-unstyled")

    events_holder.each do |event| 
        eventDetails = event.css("li").text 
        output << eventDetails 
    end 

    return output

end

